﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using state = System.String;
using alpha = System.String;

namespace RegextoNFA
{
    class NFA
    {
        public List<state> states;
        public List<alpha> alpha;
        private state mstart;
        private List<Tuple<state, alpha, state>> mtransTable;
        public List<state> final;
        public alpha Epsilon = "";

        
       
        public NFA()
        {
            states = new List<state>();
            alpha = new List<alpha>();
            mtransTable = new List<Tuple<state, alpha, state>>();
            final = new List<state>();
            alpha.Add(Epsilon);
        }
     
        public state start
        {
            

            set
            {
                   if (states.Contains(value))
                        mstart = value;
                    else
                    {
                        throw new Exception("The start state was not in the set of states.");


                    }
                
               
            }
            get { return mstart; }
        }
    
  
        public List<Tuple<state, alpha, state>> transTable
        {

            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    

                        if (states.Contains(value[i].Item1) && alpha.Contains(value[i].Item2) && states.Contains(value[i].Item3))
                            mtransTable.Add(value[i]);
                        else
                            throw new Exception("Either the state or alpha in the tfunction was not in their respective sets");
                    
                }
            }
            get { return mtransTable; }
        }
        public String run(List<String> word)
        {
            try
            {
                state currstate = start;
                for (int i = 0; i < word.Count; i++)
                {
                    if (alpha.Contains(word[i]) == false)
                        throw new Exception("One of the letters in the word is not in the alphabet.");

                }

                String s = "";
                List<state> visited = new List<state>();
                getNextStates(visited, ref s, currstate, word);


                if (s == "")
                    return "Reject";
                else
                    return s;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Regex.restart();
                return "Failure";
            }
        }

        public void getNextStates(List<state> visited ,ref String result, state currstate, List<String> word)
        {
            int i = 0;
            List<Tuple<state, alpha>> nextStates = new List<Tuple<state, alpha>>();
            if (word.Count == 0)
            {
                while (i < transTable.Count)
                {
                    if (transTable[i].Item1 == currstate &&  transTable[i].Item2 == "")
                    {
                        nextStates.Add(Tuple.Create(transTable[i].Item3, transTable[i].Item2));
                    }
                    i++;

                }
                if (checkFinal(currstate) == false && nextStates.Count != 0)
                {
                    for (int index = 0; index < nextStates.Count; index++)
                    {
                        visited.Add(nextStates[index].Item2);
                        if (nextStates[index].Item2 == "")
                            getNextStates(visited, ref result, nextStates[index].Item1, word);
                        

                    }
                }
                if (checkFinal(currstate) == true)
                {
                    result = "Accept";
                    return;
                }
                else
                    return;
            }

            nextStates.Clear();
            while (i < transTable.Count)
            {
                if (transTable[i].Item1 == currstate &&( transTable[i].Item2 == word[(word.Count - 1)] || transTable[i].Item2 == ""))
                {
                    nextStates.Add(Tuple.Create(transTable[i].Item3, transTable[i].Item2));
                }
                i++;
                
            }
            if (nextStates.Count == 0)
            {
                return;
            }
            if (visited.Count >15)
            {
                visited.Clear();
                return;
            }
            if (nextStates.Count != 0)
            {
               /* List<String> notdropWord = new List<String>();
                for(int x = 0; x < word.Count; x++)
                    notdropWord.Add(word[x]);
                if (word.Count != 0)
                {
                    word.RemoveAt(word.Count - 1);
                }
                * */
                for (int index = 0; index < nextStates.Count; index++)
                {
                    visited.Add(nextStates[index].Item2);
                    
                      // if (currstate == start)
                        //    getNextStates(visited, ref result, nextStates[index].Item1, notdropWord);

                    if (nextStates[index].Item2 != "")
                    {  List<String> a = new List<String>(word);
                            
                        if (word.Count != 0)
                        {
                            a.RemoveAt(a.Count - 1);
                        }
                        getNextStates(visited, ref result, nextStates[index].Item1, a);
                      
                    }
                    else 
                    {
                        getNextStates(visited, ref result, nextStates[index].Item1, word);
                        
                        
                    }
                        
                }
                
            }
           
            

        }
        

       
        public Boolean checkFinal(state currstate)
        {
            for (int i = 0; i < final.Count; i++)
            {
                if (currstate == final[i])
                    return true;
            }
            return false;
        }




    }
}
