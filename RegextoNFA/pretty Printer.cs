﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegextoNFA
{
    class pretty_Printer
    {
        private NFA aDfa;
        public pretty_Printer(NFA theNfa)
        {
            aDfa = new NFA();
            aDfa = theNfa;
            Dfa_printer(theNfa);
        }
        public pretty_Printer()
        {
        
        }

        public void Dfa_printer(NFA anfa)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.Write("(states, (");
            for(int i = 0; i<anfa.states.Count; i++)
            {
                
                if (i == (anfa.states.Count - 1))
                    Console.Write("["+anfa.states[i] + "]))");
                else
                Console.Write("["+anfa.states[i]+"]," );
            }
            Console.WriteLine();
            Console.Write("(alpha, (");
            foreach (var x in anfa.alpha)
            {
                if (x == "")
                { }
                if (x == "#")
                { }
                else
                {
                    Console.Write(x);
                    if (x != anfa.alpha[(anfa.alpha.Count() - 1)] && x!= anfa.alpha[0])
                        Console.Write(",");
                }
            }
            Console.Write(")");
                Console.WriteLine();
                Console.Write("(trans-func, ((");
                for (int i = 0; i < anfa.transTable.Count; i++)
                {string alpha;
                    if (anfa.transTable[i].Item2 == "#")
                         alpha = "";
                    else 
                         alpha = anfa.transTable[i].Item2 ;
                    
                    if (i == (anfa.transTable.Count - 1))
                        Console.Write("([" + anfa.transTable[i].Item1 + "]," + alpha + ",[" + anfa.transTable[i].Item3 + "])))");
                    else
                    Console.Write("([" + anfa.transTable[i].Item1 + "]," + alpha + ",[" + anfa.transTable[i].Item3 + "]),");
                }
                Console.WriteLine();
                Console.Write("(start, [");
               
                    
                    
                Console.Write(anfa.start + "])");
                
                Console.WriteLine();
                Console.Write("(final, (");
                foreach(var x in anfa.final)
                {
                    Console.Write("["+x+"]");
                    if (x != anfa.final[(anfa.final.Count() - 1)])
                    Console.Write(",");

                }
                        Console.Write(")");
                        
                }
        }

    }

