﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using state = System.String;
using alpha = System.String;

namespace RegextoNFA
{
    class Regex
    {
        private string regexx;
        public Regex()
        {
            regexx = "";

        }
        public NFA regexNFA(string regularEx)
        {
            try
            {
                

                regexx = regularEx;
                if (regexx == "")
                {
                    regexx += "#";
                }
                if (regexx == "*")
                {
                    regexx = "#";
                }
                if (regexx == "&")
                {
                    regexx = "#";
                }
                if (regexx == "@")
                {
                    regexx = "#";
                }
                if (regexx == "()")
                {
                    regexx = "#";
                }
                List<char> specialChars = new List<char> { '(', ')', '&', ' ', '@' };
                List<char> alpha = new List<char>();
                List<char> opQueue = new List<char>();
                bool paren = false;
                for (int i = 0; i < regexx.Length; i++)
                {

                    if (regexx[i] == '&') //union
                    {
                        if (i == (regexx.Length - 1))
                        {
                            //throw new Exception("There is only one argument, union requires two arguments.");
                            opQueue.Add(regexx[i]);
                            opQueue.Add('#');
                        }
                        else
                        {
                            opQueue.Add(regexx[i]);
                        }

                    }
                    else if (regexx[i] == '*') //kleene star
                    {
                        opQueue.Add(regexx[i]);

                    }
                    else if (regexx[i] == '(') // partentesis detection
                    {


                        if (checkparen(regexx) == true || paren == true)
                        {
                            paren = true;
                            opQueue.Add(regexx[i]);

                        }
                        else
                        {
                            throw new Exception(" The matching parenthesis is missing.");

                        }


                    }
                    else if (regexx[i] == ')') // partentesis detection
                    {


                        if (checkparen(regexx) == true || paren == true)
                        {
                            paren = true;
                            opQueue.Add(regexx[i]);

                        }
                        else
                        {
                            throw new Exception(" The matching parenthesis is missing.");

                        }


                    }
                    else //Concatenation
                    {
                        if (regexx[i] != ' ')
                        {
                            alpha.Add(regexx[i]);
                            if (i != 0 && specialChars.Contains(regexx[(i - 1)]) == false && specialChars.Contains(regexx[(i)]) == false)
                            {
                                opQueue.Add('@');
                            }

                            opQueue.Add(regexx[i]);
                        }
                    }


                }

                List<Tuple<NFA, char>> appQueue = new List<Tuple<NFA, char>>();
                appQueue = AppQueue(opQueue);
                NFA nullNFA = new NFA();
                nullNFA = null;
                int r = 0;
                for (int i = 0; i < appQueue.Count(); i++)
                {
                    if (appQueue[0].Item1 != null && appQueue.Count() == 1)
                        break;
                    else if (appQueue[0].Item1 != null && appQueue[1].Item2 == '*')
                    {
                        appQueue[0] = Tuple.Create(kleene(appQueue[0].Item1), ' ');
                        appQueue.RemoveAt(1);

                        if (appQueue.Count() >= 3)
                        {
                            if ((r + 1) < appQueue.Count() && specialChars.Contains(appQueue[(r + 1)].Item2) == false)
                            {
                                appQueue.Insert((r + 1), Tuple.Create(nullNFA, '@'));
                            }

                        }

                    }
                    else if (appQueue[i].Item2 == '(' && appQueue[i + 1].Item1 == null)
                    {
                        r = i + 1;
                    }

                    else if (i == (appQueue.Count() - 1))
                    {
                        if (appQueue[r].Item2 == ')')
                        {
                            appQueue.RemoveAt(r - 1);
                            appQueue.RemoveAt(r - 1);
                            r = 0;
                            i = -1;
                        }
                        else
                        {
                            int s = r;
                            List<Tuple<NFA, char>> tempqueue = new List<Tuple<NFA, char>>();
                            while (appQueue.Count() > 0 && appQueue[(r)].Item2 != ')' && appQueue[(r)].Item2 != '(')
                            {
                                tempqueue.Add(appQueue[r]);
                                appQueue.RemoveAt(r);
                            }
                            if (tempqueue.Count() > 0)
                            {

                                appQueue.Insert(r, Tuple.Create(evalLine(tempqueue), ' '));
                                if (appQueue.Count() >= 3)
                                {
                                    appQueue.RemoveAt(r - 1);
                                    appQueue.RemoveAt(r);
                                    r = r - 1;
                                    i = 0;
                                    
                                }
                              
                                        
                                   

                                if (appQueue.Count() >= 3 && r != 0)
                                {
                                    if (specialChars.Contains(appQueue[(r - 1)].Item2) == false)
                                    {
                                        appQueue.Insert(r, Tuple.Create(nullNFA, '@'));
                                        r = r + 1;
                                    }
                                    if ((r + 1) < appQueue.Count() && specialChars.Contains(appQueue[(r + 1)].Item2) == false)
                                    {
                                        appQueue.Insert((r + 1), Tuple.Create(nullNFA, '@'));
                                    }
                                }
                            }
                            i = -1;
                            r = 0;

                        }
                    }

                }
                return appQueue[0].Item1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                restart();
               return null;
            }

        }
        public static void restart()
        {
            Console.WriteLine("The rgular expression entered produced an error!");
            Console.WriteLine("Press any key to continue......");
            Console.ReadLine();
            //------------------------------------------------------------------------------
            string myApp = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;
            System.Diagnostics.Process.Start(myApp);
            Environment.Exit(0);
            //-------------------------------------------------------------------------------
            //restart code from msdn 
            //https://social.msdn.microsoft.com/Forums/vstudio/en-US/b23f784c-e5f9-4c06-baf7-aa0b3cb8254a/is-there-a-code-where-you-can-restart-a-program?forum=csharpgeneral
            //-------------------------------------------------------------------------------
                
        }
        private NFA evalLine(List<Tuple<NFA, char>> line)
        {
            try
            {
               
                NFA nullNFA = new NFA();
                nullNFA = null;
                List<char> specialChars = new List<char> { '(', ')', '*', '&', '@' };
                for (int i = 0; i < (line.Count()-1); i++)
                {
                    if (line[i].Item2 == ' ' && line[(i + 1)].Item2 == ' ')
                    {
                            if (i == 0)
                            {
                                line.Insert((i+1), Tuple.Create(nullNFA, '@'));
                            }
                            else
                            {
                                line.Insert((i +1 ), Tuple.Create(nullNFA, '@'));

                            }
                        
                        
                    }
                }
                
                Stack<Tuple<NFA, char>> stack = new Stack<Tuple<NFA, char>>();
                if (line.Contains(Tuple.Create(nullNFA, '(')) || line.Contains(Tuple.Create(nullNFA, ')')))
                {
                    throw new Exception("The evaluate function does not accept functions with parenthesis, you must remove all parenthesis before evaluating.");
                }
                while (line.Contains(Tuple.Create(nullNFA, '*')))
                {
                    for (int i = 0; i < line.Count(); i++)
                    {
                        if (line[i].Item2 == '*')
                        {
                            NFA new2 = new NFA();
                            if (line[i - 1].Item2 == '@')
                            { line.RemoveAt(i - 1);
                            i--;
                            }
                            else
                            { }
                            if (line[i - 1].Item2 == ' ')
                            { new2 = kleene((line[(i - 1)].Item1)); }
                            else
                            new2 = kleene(chartonfa(line[(i - 1)].Item2));
                            line.RemoveAt(i - 1);
                            line.RemoveAt(i - 1);
                            line.Insert((i - 1), (Tuple.Create(new2, ' ')));
                        }
                    }
                }
                if (line.Count == 2)
                {
                    if (specialChars.Contains(line[0].Item2) || specialChars.Contains(line[1].Item2))
                    { }
                    else
                        line.Insert((1), Tuple.Create(nullNFA, '@'));
                }
                for (int i = (line.Count - 1); i >= 0; i--)
                {
                    if (specialChars.Contains(line[i].Item2))
                        stack.Push(Tuple.Create(nullNFA, line[i].Item2));
                    else
                    {
                        if (line[i].Item1 == null)
                            stack.Push(Tuple.Create(chartonfa(line[i].Item2), ' '));
                        else
                            stack.Push(line[i]);
                    }
                }

                while (stack.Count() > 1)
                {

                    NFA t1 = stack.Pop().Item1;
                    char op = stack.Pop().Item2;
                    NFA t2 = new NFA();
                    if (stack.Count() != 0)
                    {

                        t2 = stack.Pop().Item1;
                    }
                    else
                    {
                        t2 = chartonfa('#');
                    }


                    if (op == '&')
                    {
                        stack.Push(Tuple.Create(Union(t1, t2), ' '));
                    }
                    if (op == '@')
                    {

                        stack.Push(Tuple.Create(concat(t1, t2), ' '));
                    }

                }
                return stack.Pop().Item1;
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                restart();
                return null;
            }

        }
        private NFA kleene(NFA t1)
        {
            try
            {
                NFA newnfa = new NFA();
                List<Tuple<state, state>> maplistt1 = new List<Tuple<state, state>>();//create list to map old values to new for t1
                int j = 0;
                newnfa.alpha = t1.alpha;
                newnfa.states.Add(j.ToString()); // add new start state
                newnfa.start = j.ToString(); //same
                newnfa.final.Add(j.ToString());
                j++;
                foreach (var y in t1.states) //add t1 states to the new NFA
                {

                    newnfa.states.Add(j.ToString()); //add the new state
                    if (t1.final.Contains(y))
                    {
                        newnfa.final.Add(j.ToString());
                    }
                    maplistt1.Add(Tuple.Create(y, j.ToString())); //map the new state to the old


                    j++;
                }
                foreach (var x in t1.transTable) //create the new transtable based off t1
                {
                    state newa = "";
                    state newb = "";
                    foreach (var y in maplistt1)
                    {
                        if (y.Item1 == x.Item1) //if the old state matches the state in the transtable
                        {
                            newa = y.Item2; //new state in new transtable is new state
                        }

                    }
                    foreach (var z in maplistt1)
                    {
                        if (z.Item1 == x.Item3)//if the old state matches the destination state in the transtable
                        {
                            newb = z.Item2; //new state in the new transtable
                        }
                    }
                    newnfa.transTable.Add(Tuple.Create(newa, x.Item2, newb)); //add the new entry to the new transtable
                }
                foreach (var x in newnfa.final)
                {
                    foreach (var z in maplistt1)
                    {
                        if (z.Item1 == t1.start)//if the old state matches the destination state in the transtable
                        {
                            newnfa.transTable.Add(Tuple.Create(x, "", z.Item2));
                        }
                    }

                }
                foreach (var z in maplistt1)
                {
                    if (z.Item1 == t1.start)//if the old state matches the destination state in the transtable
                    {
                        newnfa.transTable.Add(Tuple.Create(newnfa.start, "", z.Item2));
                    }
                }
                List<Tuple<state, alpha, state>> newlist = newnfa.transTable.Distinct().ToList();
                newnfa.transTable.RemoveRange(0, newnfa.transTable.Count());
                newnfa.transTable = newlist;
                return newnfa;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
        private bool checkparen(string s)
        {
            try
            {
                var stack = new Stack<int>();
                int j = 0;
                for (int i = 0; i < s.Length; i++)
                {
                    switch (s[i])
                    {
                        case '(':
                            j++;
                            break;
                        case ')':
                            j--;
                            break;
                        default:
                            break;
                    }
                }
                if (j == 0)
                {
                    return true;
                }
                else
                {
                    if (j < 0)
                        throw new Exception("There are too many closing parenthesis.");
                    if (j > 0)
                        throw new Exception("There are too many opening parenthesis.");
                    return false;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                restart();
                return false;
            }
        }
        private NFA concat(NFA t1, NFA t2)
        {
            try
            {
                if (t1 == null)
                    throw new Exception("T1 cannot be null");
                if (t2 == null)
                    throw new Exception("T2 cannot be null");
                NFA newnfa = new NFA();
                List<Tuple<state, state>> maplistt1 = new List<Tuple<state, state>>();//create list to map old values to new for t1
                int j = 0;



                foreach (var y in t1.states) //add t1 states to the new NFA
                {

                    newnfa.states.Add(j.ToString()); //add the new state
                    maplistt1.Add(Tuple.Create(y, j.ToString())); //map the new state to the old


                    j++;
                }
                foreach (var x in t1.transTable) //create the new transtable based off t1
                {
                    state newa = "";
                    state newb = "";
                    foreach (var y in maplistt1)
                    {
                        if (y.Item1 == x.Item1) //if the old state matches the state in the transtable
                        {
                            newa = y.Item2; //new state in new transtable is new state
                        }

                    }
                    foreach (var z in maplistt1)
                    {
                        if (z.Item1 == x.Item3)//if the old state matches the destination state in the transtable
                        {
                            newb = z.Item2; //new state in the new transtable
                        }
                    }
                    newnfa.transTable.Add(Tuple.Create(newa, x.Item2, newb)); //add the new entry to the new transtable
                }
                List<Tuple<state, state>> maplistt2 = new List<Tuple<state, state>>();
                foreach (var y in t2.states)
                {

                    newnfa.states.Add(j.ToString());

                    maplistt2.Add(Tuple.Create(y, j.ToString()));


                    j++;
                }
                foreach (var x in t2.transTable)
                {
                    state newa = "";
                    state newb = "";
                    foreach (var y in maplistt2)
                    {
                        if (y.Item1 == x.Item1)
                        {
                            newa = y.Item2;
                        }
                    }
                    foreach (var z in maplistt2)
                    {
                        if (z.Item1 == x.Item3)
                        {
                            newb = z.Item2;
                        }
                    }
                    if (newa != "" && newb != "")
                        newnfa.transTable.Add(Tuple.Create(newa, x.Item2, newb));
                }
                foreach (var x in t1.final)
                {
                    foreach (var y in maplistt1)
                    {
                        if (y.Item1 == x)
                        {
                            foreach (var z in maplistt2)
                            {
                                if (z.Item1 == t2.start)
                                {
                                    newnfa.transTable.Add(Tuple.Create(y.Item2, "", z.Item2));
                                }
                            }
                        }
                    }
                }
                foreach (var x in t1.alpha)
                {
                    if (newnfa.alpha.Contains(x) == false)
                        newnfa.alpha.Add(x);
                }
                foreach (var x in t2.alpha)
                {
                    if (newnfa.alpha.Contains(x) == false)
                        newnfa.alpha.Add(x);
                }
                foreach (var x in maplistt1)
                {
                    if (x.Item1 == t1.start)
                        newnfa.start = x.Item2; //same
                }
                foreach (var x in maplistt2)
                {
                    if (t2.final.Contains(x.Item1))
                        newnfa.final.Add(x.Item2);
                }
                return newnfa;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                restart();
                return null;
            }

        }
        private NFA chartonfa(char state)
        {
            NFA newnfa = new NFA();
            newnfa.states.Add("0");
            newnfa.states.Add("1");
            newnfa.start = "0";
            newnfa.final.Add("1");
            newnfa.alpha.Add(state.ToString());
            newnfa.transTable.Add(Tuple.Create("0", state.ToString(), "1"));
            return newnfa;


        }

        private NFA Union(NFA t1, NFA t2)
        {
            NFA newnfa = new NFA();
            return addstates(t1, t2, newnfa);


        }
        private NFA addstates(NFA t1, NFA t2, NFA newnfa)
        {
            try
            {
                List<Tuple<state, state>> maplistt1 = new List<Tuple<state, state>>();//create list to map old values to new for t1
                int j = 0;
                newnfa.states.Add(j.ToString()); // add new start state
                newnfa.start = j.ToString(); //same
                j++;
                foreach (var y in t1.states) //add t1 states to the new NFA
                {

                    newnfa.states.Add(j.ToString()); //add the new state
                    if (t1.final.Contains(y))
                    {
                        newnfa.final.Add(j.ToString());
                    }
                    maplistt1.Add(Tuple.Create(y, j.ToString())); //map the new state to the old


                    j++;
                }
                foreach (var x in t1.transTable) //create the new transtable based off t1
                {
                    state newa = "";
                    state newb = "";
                    foreach (var y in maplistt1)
                    {
                        if (y.Item1 == x.Item1) //if the old state matches the state in the transtable
                        {
                            newa = y.Item2; //new state in new transtable is new state
                        }

                    }
                    foreach (var z in maplistt1)
                    {
                        if (z.Item1 == x.Item3)//if the old state matches the destination state in the transtable
                        {
                            newb = z.Item2; //new state in the new transtable
                        }
                    }
                    newnfa.transTable.Add(Tuple.Create(newa, x.Item2, newb)); //add the new entry to the new transtable
                }
                List<Tuple<state, state>> maplistt2 = new List<Tuple<state, state>>();
                foreach (var y in t2.states)
                {

                    newnfa.states.Add(j.ToString());
                    if (t2.final.Contains(y))
                    {
                        newnfa.final.Add(j.ToString());
                    }
                    maplistt2.Add(Tuple.Create(y, j.ToString()));


                    j++;
                }
                foreach (var x in t2.transTable)
                {
                    state newa = "";
                    state newb = "";
                    foreach (var y in maplistt2)
                    {
                        if (y.Item1 == x.Item1)
                        {
                            newa = y.Item2;
                        }
                    }
                    foreach (var z in maplistt2)
                    {
                        if (z.Item1 == x.Item3)
                        {
                            newb = z.Item2;
                        }
                    }
                    if (newa != "" && newb != "")
                        newnfa.transTable.Add(Tuple.Create(newa, x.Item2, newb));
                }
                foreach (var x in maplistt1)
                {
                    if (x.Item1 == t1.start)
                        newnfa.transTable.Add(Tuple.Create(newnfa.start, "", x.Item2));
                }
                foreach (var x in maplistt2)
                {
                    if (x.Item1 == t2.start)
                        newnfa.transTable.Add(Tuple.Create(newnfa.start, "", x.Item2));
                }
                foreach (var x in t1.alpha)
                {
                    if (newnfa.alpha.Contains(x) == false)
                        newnfa.alpha.Add(x);
                }
                foreach (var x in t2.alpha)
                {
                    if (newnfa.alpha.Contains(x) == false)
                        newnfa.alpha.Add(x);
                }
                return newnfa;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                restart();
                return null;
            }
        }


        private List<Tuple<NFA, char>> AppQueue(List<char> opQueue)
        {
            try
            {
                List<Tuple<NFA, char>> appedQueue = new List<Tuple<NFA, char>>();
                List<char> specialChars = new List<char> { '(', ')', '*', '&', ' ', '@' };
                NFA nullNFA = new NFA();
                nullNFA = null;
                foreach (var x in opQueue)
                    appedQueue.Add(Tuple.Create(nullNFA, x));
                return appedQueue;
            }


            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                restart();
                return null;
            }
        }
    }
         }

        

